package main

import (
	"log"
	"os"
	"os/exec"

	"github.com/pkg/errors"
	"gitlab.com/tedwexler/inception/internal"
)

var repositorySettings = []internal.RepositoryConfiguration{
	{
		Slug: "twexler/init.vim",
		SymlinkDestinations: []internal.SymlinkPath{
			{
				Path: ".config/nvim",
			},
		},
		Kind: internal.KindGithubRepo,
	},
	{
		Slug: "twexler/zshrc",
		SymlinkDestinations: []internal.SymlinkPath{
			{
				SubPath: "zshrc",
				Path:    ".zshrc",
			},
		},
		Kind: internal.KindGithubRepo,
	},
	{
		Slug: "6dcdbb2bc376d398775c323126349580",
		SymlinkDestinations: []internal.SymlinkPath{
			{
				SubPath: "starship.toml",
				Path:    ".config/starship.toml",
			},
		},
		Kind: internal.KindGithubGist,
	},
	{
		Slug:      "twexler/Brewfile",
		Platforms: []string{"darwin"},
		Kind:      internal.KindGithubRepo,
		PostRun: func(wd string) error {
			log.Printf("running brew bundle in %s", wd)
			brew, err := exec.LookPath("brew")
			if err != nil {
				return errors.Wrap(err, "unable to find brew")
			}
			cmd := exec.Command(brew, "bundle")
			cmd.Dir = wd
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			if err := cmd.Run(); err != nil {
				return errors.Wrap(err, "unable to run brew bundle")
			}
			return nil
		},
	},
	{
		Slug: "twexler/.tmux",
		SymlinkDestinations: []internal.SymlinkPath{
			{
				Path:    ".tmux.conf",
				SubPath: ".tmux.conf",
			},
			{
				Path:    ".tmux.conf.local",
				SubPath: ".tmux.conf.local",
			},
		},
	},
}
