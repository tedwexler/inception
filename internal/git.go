package internal

import (
	"context"
	"log"
	"os"

	"github.com/go-git/go-git/v5"
	"github.com/pkg/errors"
)

func cloneGitRepository(ctx context.Context, repoURL, destination string) error {
	log.Printf("%s does not exist, cloning %s", destination, repoURL)
	_, err := git.PlainCloneContext(ctx, destination, false, &git.CloneOptions{
		URL:      repoURL,
		Progress: os.Stdout,
	})
	if err != nil {
		return errors.Wrapf(err, "unable to clone %s", repoURL)
	}
	return nil
}

func updateGitRepository(ctx context.Context, destination string) error {
	repo, err := git.PlainOpen(destination)
	if err != nil {
		return errors.Wrapf(err, "unable to initialize repository at %s", destination)
	}
	wt, err := repo.Worktree()
	if err != nil {
		return errors.Wrapf(err, "unable to get git worktree at %s", destination)
	}
	if err := wt.PullContext(ctx, &git.PullOptions{RemoteName: "origin", Progress: os.Stdout}); err != nil {
		return errors.Wrapf(err, "unable to pull repository at %s", destination)
	}
	return nil
}
