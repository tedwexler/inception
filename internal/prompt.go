package internal

import (
	"log"

	survey "github.com/AlecAivazis/survey/v2"
	"github.com/AlecAivazis/survey/v2/core"
	"github.com/AlecAivazis/survey/v2/terminal"
	"github.com/pkg/errors"
)

type promptAndRenderer interface {
	survey.Prompt
	AppendRenderedText(text string)
	Error(config *survey.PromptConfig, invalid error) error
	NewCursor() *terminal.Cursor
	NewRuneReader() *terminal.RuneReader
	OffsetCursor(offset int)
	Render(tmpl string, data interface{}) error
	RenderWithCursorOffset(tmpl string, data survey.IterableOpts, opts []core.OptionAnswer, idx int) error
	Stdio() terminal.Stdio
	WithStdio(stdio terminal.Stdio)
}

type maxTriesPrompt struct {
	promptAndRenderer
	maxTries int
}

func (mtp maxTriesPrompt) Prompt(config *survey.PromptConfig) (ans interface{}, err error) {
	for i := 0; i <= mtp.maxTries; i++ {
		ans, err = mtp.promptAndRenderer.Prompt(config)
		if err != nil {
			return nil, err
		}
		if err = survey.Required(ans); err != nil {
			log.Printf("invalid input: %s", err.Error())
			continue
		}
		break
	}
	if err != nil {
		return nil, errors.Wrapf(err, "invalid input, max attempts (%d)", mtp.maxTries)
	}
	return ans, nil
}

func (mtp maxTriesPrompt) Cleanup(config *survey.PromptConfig, ans interface{}) error {
	return mtp.promptAndRenderer.Cleanup(config, ans)
}

func (mtp maxTriesPrompt) Error(config *survey.PromptConfig, err error) error {
	return mtp.promptAndRenderer.Cleanup(config, err)
}
