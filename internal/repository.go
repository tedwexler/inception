package internal

import "time"

type RepoKind int

const (
	KindGithubRepo RepoKind = iota
	KindGithubGist
)

type RepositoryConfiguration struct {
	Slug                string
	SymlinkDestinations []SymlinkPath
	Kind                RepoKind
	PostRun             func(string) error
	Platforms           []string
	Timeout             time.Duration
}

type SymlinkPath struct {
	Path    string
	SubPath string
}
