package internal

import (
	"context"
	"log"
	"os"
	"path"
	"strings"

	"github.com/AlecAivazis/survey/v2"
	"github.com/google/go-github/v39/github"
	"github.com/pkg/errors"
	"golang.org/x/oauth2"
)

func ConfigureForGithub(ctx context.Context) (*github.Client, error) {
	githubAPIToken := os.Getenv("INCEPTION_GITHUB_TOKEN")
	if githubAPIToken == "" {
		prompt := &maxTriesPrompt{
			promptAndRenderer: &survey.Password{
				Message: "Github API Token",
			},
			maxTries: 5,
		}
		if err := survey.AskOne(prompt, &githubAPIToken); err != nil {
			return nil, errors.Wrap(err, "invalid input")
		}
	}

	// configure github OAuth
	tokenSource := oauth2.StaticTokenSource(&oauth2.Token{AccessToken: githubAPIToken})
	tokenClient := oauth2.NewClient(ctx, tokenSource)
	githubClient := github.NewClient(tokenClient)

	user, _, err := githubClient.Users.Get(ctx, "")
	if err != nil {
		return nil, errors.Wrap(err, "unable to authenticate with github")
	}
	log.Printf("authenticated %s with github", *user.Login)
	return githubClient, nil
}

func GithubRepository(ctx context.Context, githubClient *github.Client, repository RepositoryConfiguration, update bool, inceptionDir, homeDir string) (string, error) {
	repoParts := strings.Split(repository.Slug, "/")
	repo, _, err := githubClient.Repositories.Get(ctx, repoParts[0], repoParts[1])
	if err != nil {
		return "", errors.Wrapf(err, "unable to fetch repository: %s", repository.Slug)
	}

	inceptionRepoDir, err := createOrUpdateInceptionRepo(ctx, inceptionDir, repository.Slug, repo.GetSSHURL(), update)
	if err != nil {
		return "", errors.Wrap(err, "error working with repository")
	}

	for _, repoSymlink := range repository.SymlinkDestinations {
		symlinkPath := path.Join(homeDir, repoSymlink.Path)

		symlinkTarget := inceptionRepoDir
		if repoSymlink.SubPath != "" {
			// append the subpath to the repository's directory
			symlinkTarget = path.Join(symlinkTarget, repoSymlink.SubPath)
		}
		ok, err := isSymlinkInceptionControlled(symlinkPath, symlinkTarget)
		if err != nil {
			return "", errors.Wrapf(err, "unable to determine state of %s", symlinkPath)
		}
		if !ok {
			if symlinkPath != "" {
				if err := os.Symlink(symlinkTarget, symlinkPath); err != nil {
					return "", errors.Wrapf(err, "unable to create symlink from %s to %s", inceptionRepoDir, symlinkPath)
				}
			}
		}
	}

	return inceptionRepoDir, nil
}

func GithubGist(ctx context.Context, githubClient *github.Client, repository RepositoryConfiguration, update bool, inceptionDir, homeDir string) error {
	gist, _, err := githubClient.Gists.Get(ctx, repository.Slug)
	if err != nil {
		return errors.Wrapf(err, "unable to fetch github gist with id %s", repository.Slug)
	}

	inceptionGistDir, err := createOrUpdateInceptionRepo(ctx, inceptionDir, repository.Slug, gist.GetGitPullURL(), update)
	if err != nil {
		return errors.Wrap(err, "error working with repository")
	}

	for _, repoSymlink := range repository.SymlinkDestinations {
		if repoSymlink.SubPath == "" {
			log.Printf("missing subpath configuration for gist id %s (%s)", repository.Slug, repoSymlink.Path)
			continue
		}

		symlinkPath := path.Join(homeDir, repoSymlink.Path)
		inceptionGistPath := path.Join(inceptionGistDir, repoSymlink.SubPath)
		ok, err := isSymlinkInceptionControlled(symlinkPath, inceptionGistPath)
		if err != nil {
			return errors.Wrapf(err, "unable to determine state of %s", symlinkPath)
		}
		if !ok {
			if symlinkPath != "" {
				if err := os.Symlink(inceptionGistPath, symlinkPath); err != nil {
					return errors.Wrapf(err, "unable to create symlink from %s to %s", inceptionGistPath, symlinkPath)
				}
			}

		}
	}

	return nil
}
