package internal

import (
	"context"
	"os"
	"testing"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/config"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const basicRepoURL = "https://github.com/git-fixtures/basic"

func withTempDir(t *testing.T, f func(path string)) {
	dir, err := os.MkdirTemp(os.TempDir(), "inception-test")
	if err != nil {
		require.NoError(t, err)
	}
	defer os.RemoveAll(dir)
	f(dir)
}

func TestCloneGitRepository(t *testing.T) {
	ctx := context.Background()
	tests := []struct {
		url       string
		expectErr bool
	}{
		{
			url:       basicRepoURL,
			expectErr: false,
		},
		{
			url:       "not-a-url",
			expectErr: true,
		},
	}
	for _, test := range tests {
		t.Run(test.url, func(subT *testing.T) {
			withTempDir(t, func(path string) {
				err := cloneGitRepository(ctx, test.url, path)
				if test.expectErr {
					assert.Error(t, err)
				} else {
					assert.NoError(t, err)
				}
			})

		})
	}
}

func TestUpdateGitRepository(t *testing.T) {
	ctx := context.Background()
	tests := []struct {
		createRepo, createOrigin, expectErr bool
	}{
		{
			createRepo:   true,
			createOrigin: true,
			expectErr:    false,
		},
		{
			createRepo: false,
			expectErr:  true,
		},
		{
			createRepo:   true,
			createOrigin: false,
			expectErr:    true,
		},
	}
	for _, test := range tests {
		withTempDir(t, func(path string) {
			var repo *git.Repository
			var err error
			if test.createRepo {
				repo, err = git.PlainInit(path, false)
				require.NoError(t, err)
			}
			if test.createOrigin {
				repo.CreateRemote(&config.RemoteConfig{
					Name: git.DefaultRemoteName,
					URLs: []string{
						basicRepoURL,
					},
				})
			}
			err = updateGitRepository(ctx, path)
			if test.expectErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}
