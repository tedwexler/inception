package internal

import (
	"context"
	"fmt"
	"os"
	"path"
	"path/filepath"

	"github.com/pkg/errors"
)

func createOrUpdateInceptionRepo(ctx context.Context, inceptionDir, slug, pullURL string, update bool) (string, error) {

	repoDir := path.Join(inceptionDir, slug)

	if err := ensureInceptionRepo(ctx, pullURL, repoDir); err != nil {
		return "", errors.Wrapf(err, "unable to create inception gist dir: %s", repoDir)
	}

	if update {
		return repoDir, updateGitRepository(ctx, repoDir)
	}

	return repoDir, nil
}

func ensureInceptionRepo(ctx context.Context, cloneURL, destination string) error {
	_, err := os.Stat(destination)
	if os.IsNotExist(err) {
		if err := cloneGitRepository(ctx, cloneURL, destination); err != nil {
			return errors.Wrap(err, "unable to clone repository")
		}
	} else if err != nil {
		return errors.Wrapf(err, "unable to stat %s", destination)
	}
	return nil
}

func isSymlinkInceptionControlled(symlinkPath, destination string) (bool, error) {
	if symlinkPath == "" {
		// this file doesn't have a symlink, so always return false
		return false, nil
	}
	stat, err := os.Lstat(symlinkPath)
	if os.IsNotExist(err) {
		// it doesn't exist, so we should create it
		return false, nil
	} else if err != nil {
		return false, errors.Wrap(err, "unable to stat")
	}
	if stat.Mode()&os.ModeSymlink != 0 {
		// it's a symlink...
		path, err := filepath.EvalSymlinks(symlinkPath)
		if err != nil {
			return false, errors.Wrapf(err, "unable to resolve symlink %s", symlinkPath)
		}
		if path == destination {
			return true, nil
		}
	}

	return false, fmt.Errorf("%s exists, but is not controlled by inception", symlinkPath)
}
