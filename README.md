inception
=========

`inception` is a tool for bootstrapping a new computer. I've written it to support my own use cases, since I'm expecting to have to bootstrap a few new machines in the near future

Ironically, `inception` currently only supports resources on GitHub, but that will change soon.

`inception` requires a GitHub personal access token with the `:repo` scope. It can be set in the environment as `INCEPTION_GITHUB_TOKEN`, or `inception` will prompt you to enter one when running


This application is a toy, mostly. Please don't expect the nicest code, or commit history!
