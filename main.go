package main

import (
	"context"
	"flag"
	"log"
	"os"
	"path"
	"runtime"
	"sync"
	"time"

	"github.com/adrg/xdg"
	"github.com/google/go-github/v39/github"
	"github.com/pkg/errors"
	"gitlab.com/tedwexler/inception/internal"
)

var (
	homeArg      *string
	inceptionDir = ""
	update       = flag.Bool("update", false, "update existing repositories in place")
)

func init() {
	homeDir, err := os.UserHomeDir()
	if err != nil {
		log.Printf("unable to determine home directory: %s", err.Error())
		os.Exit(1)
	}
	homeArg = flag.String("home", homeDir, "specify a home directory to work in")
}

func main() {
	flag.Parse()

	// this is a big fat hack for debugging, sorry.
	oldHome := os.Getenv("HOME")
	os.Setenv("HOME", *homeArg)
	xdg.Reload()
	os.Setenv("HOME", oldHome)

	log.Printf("working in %s", *homeArg)
	if err := setupInceptionDir(); err != nil {
		log.Printf("unable to create inception dir: %s", err)
		os.Exit(1)
	}

	ctx := context.Background()

	var githubClient *github.Client
	var err error
	githubCtx, githubCancel := context.WithTimeout(ctx, time.Minute)

	if githubClient, err = internal.ConfigureForGithub(githubCtx); err != nil {
		log.Printf("unable to configure inception for github: %s", err.Error())
		os.Exit(1)
	}
	githubCancel()

	wg := sync.WaitGroup{}
	for _, repository := range repositorySettings {

		if !checkPlatform(repository.Platforms) {
			log.Printf("%s is not supported on this platform (supports %+v)", repository.Slug, repository.Platforms)
			continue
		}

		if repository.Timeout == 0 {
			repository.Timeout = time.Minute
		}

		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, repository.Timeout)

		switch repository.Kind {
		case internal.KindGithubRepo:
			log.Printf("new github repo: %s", repository.Slug)
			wg.Add(1)
			go func(repository internal.RepositoryConfiguration) {
				defer cancel()
				defer wg.Done()
				var inceptionDir string
				var err error
				if inceptionDir, err = internal.GithubRepository(ctx, githubClient, repository, *update, inceptionDir, *homeArg); err != nil {
					log.Printf("error fetching github repository %s: %s", repository.Slug, err.Error())
					return
				}
				if repository.PostRun != nil {
					if err := repository.PostRun(inceptionDir); err != nil {
						log.Printf("error running postRun on %s: %s", repository.Slug, err.Error())
					}
				}
			}(repository)
		case internal.KindGithubGist:
			log.Printf("new github gist: %s", repository.Slug)
			wg.Add(1)
			go func(repository internal.RepositoryConfiguration) {
				defer cancel()
				defer wg.Done()
				if err := internal.GithubGist(ctx, githubClient, repository, *update, inceptionDir, *homeArg); err != nil {
					log.Printf("error fetching github gist %s: %s", repository.Slug, err.Error())
				}
			}(repository)
		default:
			log.Printf("unknown repository type for %s", repository.Slug)
			continue
		}
	}
	wg.Wait()
}

func setupInceptionDir() error {
	inceptionDir = path.Join(xdg.ConfigHome, "inception")
	_, err := os.Stat(inceptionDir)
	if os.IsNotExist(err) {
		log.Printf("%s does not exist, creating it", inceptionDir)
		err := os.Mkdir(inceptionDir, 0700)
		if err != nil {
			return errors.Wrapf(err, "unable to create inception working dir at %s", inceptionDir)
		}
	} else if err != nil {
		return errors.Wrapf(err, "unable to stat %s", inceptionDir)
	}
	return nil
}

func checkPlatform(platforms []string) bool {
	foundPlatform := len(platforms) == 0
	for _, platform := range platforms {
		if platform == runtime.GOOS {
			foundPlatform = true
			break
		}
	}
	return foundPlatform
}
